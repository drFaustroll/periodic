#!/usr/bin/env python3

f=open("raw-state",'r')

atoms={}
kJmol2eV=0.01036410

a = f.read().split("\n")
for l in a[1:]:
    if len(l)>0:
      x=l.split("|")
      z=int(x[1])
      name=x[2].strip()
      S=x[3].strip()
      state=x[4].strip()
      lab = False
      if x[5].strip() == 'y':
         lab = True
      rad = False
      if x[6].strip() == 'y':
         rad = True
      atoms[z]={'Z':z, 'name': name,'state': state,'symbol':S,'lab':lab,'radio':rad}
f.close()

f=open("raw-config",'r')
a = f.read().split("\n")
for l in a[1:]:
    if len(l)>0:
      x=l.split("|")
      z=int(x[1])
      elec=x[3].strip()
      if x[4].strip() == 'Unknown':
         density = "U"
      else:
         density=float(x[4])
      atoms[z].update({'config':elec,'density':density,'mass':x[5].strip()})
f.close()

f=open("raw-atomic",'r')
a = f.read().split("\n")
for l in a[1:]:
    if len(l)>0:
      x=l.split("|")
      z=int(x[1])
      if x[3].strip() == 'Unknown':
         rad = "U"
      else:
         rad=float(x[3])
      atoms[z].update({'atomic_radius':rad})
f.close()

f=open("raw-affinity",'r')
a = f.read().split("\n")
for l in a[1:]:
    if len(l)>0:
      x=l.split("|")
      z=int(x[1])
      if x[3].strip() in ['Unknown','Not stable']:
         aff = ''.join( [z[0] for z in x[3].strip().split()] )
      else:
         aff=float(x[3])*kJmol2eV
      atoms[z].update({'affinity':aff})

f=open("raw-ionicity",'r')
a = f.read().split("\n")
for l in a[1:]:
    if len(l)>0:
      x=l.split("|")
      z=int(x[1])
      if x[3].strip() == '-':
         ion = "U"
      else:
         ion=float(x[3])*kJmol2eV
      atoms[z].update({'ionicity':ion})
f.close()

f=open("raw-oxidation",'r')
a = f.read().split("\n")
for l in a[1:]:
    if len(l)>0:
      x=l.split("|")
      z=int(x[1])
      if x[3].strip() == 'Unknown':
         oxi = "U"
      else:
         oxi=x[3].strip()
      atoms[z].update({'oxidation':oxi})
f.close()
lat={}
for k in atoms:
    a=atoms[k]
    ex="\\"+a['state'].lower()
    if a['state']=='Unknown':
        ex="\\unk"
    lab=""
    if a['lab']:
       lab="\\lab"
    rad=''
    if a['radio']:
       lab="\\radio"

    el="{{\Elem{{{} {}{}{}}}{{{}}}{{{}}}{{{}}}{{{}}}{{{:.4}}}{{${}$}}{{{:.4}}}{{{:.4}}}}};".format(k,ex,lab,rad,a['mass'],a['symbol'],a['name'],a['oxidation'],a['ionicity'],a['config'],
        a['atomic_radius'],a['affinity'])
    lat['('+a['symbol']+')']=el

keys = lat.keys()
f=open('periodic-template.tex','r')
g=open('periodic-table.tex','w')
for line in f:
    ll = line.strip("\n")
    if len(ll)>4:
        x=ll[-4:].strip()
        if x in keys:
            ll+=lat[x]
    print(ll,file=g)
f.close()
g.close()
