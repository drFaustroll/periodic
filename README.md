raw data is from https://www.rsc.org/periodic-table

atoms.py will generate a periodic-table.tex based on the raw data by using periodic-template.tex
a0-a5 can be produced by using periodic-table-aX.tex

```bash

lualatex periodic-table.tex
lualatex periodic-table-a0.tex
lualatex periodic-table-a1.tex
lualatex periodic-table-a2.tex
lualatex periodic-table-a3.tex
lualatex periodic-table-a4.tex
lualatex periodic-table-a5.tex
```

![periodic table sample](periodic-table.png)


